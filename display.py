from pygame.locals import *
import datetime
import os
import pygame
import sys
import time
import math
import urllib
os.environ["SDL_FBDEV"] = "/dev/fb1"


class Display(object):

    PI = math.pi

    SMALL_FONT = 30
    MED_FONT = 50

    BLACK = (0, 0, 0)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    CYAN = (0, 255, 255)
    MAGENTA = (255, 0, 255)
    YELLOW = (255, 225, 0)
    WHITE = (255, 255, 255)

    def __init__(self, surface):
        super(Display, self).__init__()
        print("pygame init")
        pygame.init()

        # Set up the screen

        print("Set up the screen")
        self.surface = surface
        pygame.mouse.set_visible(0)

    def loop(self):
        print("main loop")
        while True:

            black_square_that_is_the_size_of_the_screen = pygame.Surface(self.surface.get_size())
            black_square_that_is_the_size_of_the_screen.fill(Display.BLACK)
            self.surface.blit(black_square_that_is_the_size_of_the_screen, (0, 0))

            # Draw time
            # font = pygame.font.Font(None, Display.SMALL_FONT)
            # textSurface = font.render("000000", 1, Display.WHITE)
            # textCenter = (W / 2, H - (textSurface.get_height() / 2))
            # textpos = textSurface.get_rect(center=textCenter)
            # self.surface.blit(textSurface, textpos)

            # Draw speed arc
            arcX = self.surface.get_width() / 4
            arcY = self.surface.get_height() / 8
            arcW = self.surface.get_width() / 2
            arcH = self.surface.get_height() / 1.33
            pygame.draw.arc(self.surface, Display.BLUE, [arcX, arcY, arcW, arcH], 5.75, 3.66, 1)

            # Draw speed needle

            # Draw speed text
            font = pygame.font.Font(None, Display.MED_FONT)
            speedStr = "42 mph"
            textSurface = font.render(speedStr, 1, Display.WHITE)

            textCenterX = self.surface.get_width() / 2
            textCenterY = (self.surface.get_height() / 2) + 45
            textCenter = (textCenterX, textCenterY)
            textpos = textSurface.get_rect(center=textCenter)

            self.surface.blit(textSurface, textpos)

            # draw temp 1 arc
            temp1ArcX = 2
            temp1ArcY = self.surface.get_height() / 16
            temp1ArcW = self.surface.get_width() / 4
            temp1ArcH = self.surface.get_height() / 2.66
            pygame.draw.arc(self.surface, Display.BLUE, [temp1ArcX, temp1ArcY, temp1ArcW, temp1ArcH], 5.75, 3.66, 1)

            # draw temp 1 text
            temp1Font = pygame.font.Font(None, Display.SMALL_FONT)
            temp1Str = "300 F"
            temp1TextSurface = temp1Font.render(temp1Str, 1, Display.WHITE)

            temp1TextCenterX = self.surface.get_width() / 8
            temp1TextCenterY = (self.surface.get_height() / 4) + 20
            temp1TextCenter = (temp1TextCenterX, temp1TextCenterY)
            temp1Textpos = temp1TextSurface.get_rect(center=temp1TextCenter)

            self.surface.blit(temp1TextSurface, temp1Textpos)

            # draw temp 2 arc
            temp2ArcX = self.surface.get_width() - (self.surface.get_width() / 4)
            temp2ArcY = self.surface.get_height() / 16
            temp2ArcW = self.surface.get_width() / 4
            temp2ArcH = self.surface.get_height() / 2.66
            pygame.draw.arc(self.surface, Display.BLUE, [temp2ArcX, temp2ArcY, temp2ArcW, temp2ArcH], 5.75, 3.66, 1)

            # draw temp 2 text
            temp2Font = pygame.font.Font(None, Display.SMALL_FONT)
            temp2Str = "451 F"
            temp2TextSurface = temp2Font.render(temp2Str, 1, Display.WHITE)

            temp2TextCenterX = self.surface.get_width() - (self.surface.get_width() / 8)
            temp2TextCenterY = (self.surface.get_height() / 4) + 20
            temp2TextCenter = (temp2TextCenterX, temp2TextCenterY)
            temp2Textpos = temp2TextSurface.get_rect(center=temp2TextCenter)

            self.surface.blit(temp2TextSurface, temp2Textpos)

            # Update the screen
            pygame.display.update()


if __name__ == '__main__':
    W = 480
    H = 320
    DISPLAYSURF = pygame.display.set_mode((W, H), 0, 16)
    display = Display(DISPLAYSURF)
    display.loop()
